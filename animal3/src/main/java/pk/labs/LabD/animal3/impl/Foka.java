package pk.labs.LabD.animal3.impl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import org.osgi.service.component.ComponentContext;
import pk.labs.LabD.contracts.Animal;
import pk.labs.LabD.contracts.Logger;

/**
 *
 * @author st
 */
public class Foka implements Animal{
     private String species;
    private String status;
    private String name;
    private Logger logger;
    private PropertyChangeSupport pcs = new PropertyChangeSupport(this);
     private ComponentContext context;
    public Foka(){
        this.species="Foka";
        this.name="Fela";
    }
     @Override
    public String getSpecies() {
       return species;
    }

    @Override
    public String getName() {
       return name;
    }

    @Override
    public String getStatus() {
       return status;
    }

    @Override
    public void setStatus(String status) {
      this.pcs.firePropertyChange("zmiana statusu", this.status, status);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
       this.pcs.removePropertyChangeListener(listener);
    } 
     public void setLogger(Logger l){
        this.logger=l;
    }
    
    protected void activate(ComponentContext ctxt) {
       this.context=ctxt;
       logger.log(this, "Foka przyszła");
       
        
    }
    protected void deactivate(ComponentContext ctxt) {
        this.context=null;
        logger.log(this, "Foka poszła");
    }
    
}
